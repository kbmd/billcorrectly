unit BillThisEtc;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls;

type

  { TBillThisEtcForm }

  TBillThisEtcForm = class(TForm)
    BillThisLabel: TLabel;
    DocumentationHintsMemo: TMemo;
    DebuggingInfoMemo: TMemo;
    procedure DebuggingInfoMemoChange(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  BillThisEtcForm: TBillThisEtcForm;

implementation

{$R *.lfm}

{ TBillThisEtcForm }

procedure TBillThisEtcForm.DebuggingInfoMemoChange(Sender: TObject);
begin

end;

end.

