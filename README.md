# README #

BillCorrectly

## NOTE: CHANGES IN CMMS BILLING RULES OVER TIME HAVE MADE THIS TOOL INCORRECT ##

### What is this repository for? ###

* **Quick Summary:** This is my attempt to actually follow the rules my business office handed me for billing E&M codes in my group psychiatry practice.
* A further description is available on [Authorea](https://www.authorea.com/users/4510/articles/126197/_show_article).
* **Usual disclaimers:** The software's only intended purpose is to help the user bill correctly. I do not claim that this software works correctly. I don't take responsibility if CMS.gov or anyone else comes after you because you rely on it. Etc.
* Written in [Free Pascal](http://www.freepascal.org/) using [Lazarus](http://www.lazarus-ide.org/), which are available on Windows, Mac OS X, and Linux with a goal to ["write once, compile anywhere"](http://wiki.freepascal.org/Lazarus_Faq#What_is_Lazarus.3F).
* Version 0.2
* BSD license (see LICENSE.TXT)

### Contribution guidelines ###
* Feel free to submit improvements.
* Don't complain. I don't have time for this really.

### Who do I talk to? ###

* [Kevin J. Black, M.D.](mailto:kbmd@byu.net)


## YTBD ... ##

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###
* Writing tests
* Code review