unit BillCorrectlyEntry;

{$DEFINE DEBUG}
// {$UNDEF  DEBUG}

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  ExtCtrls,
  BillThisEtc
  ;

type

  { TBillCorrectlyEntryForm }

  TBillCorrectlyEntryForm = class(TForm)
    AcuteUncomplicatedIllness: TCheckBox;
    ResetEntriesButton: TButton;
    FaceToFaceMinChooser: TListBox;
    OTCdrugs: TCheckBox;
    SetDefaultsButton: TButton;
    ShowMeButton: TButton;
    ChiefComplaint: TCheckBox;
    ClearAllEntriesButton: TButton;
    StrengthTone: TCheckBox;
    Gait: TCheckBox;
    Station: TCheckBox;
    attention: TCheckBox;
    orientation: TCheckBox;
    speech: TCheckBox;
    STM_LTM: TCheckBox;
    language: TCheckBox;
    intellect_FoK: TCheckBox;
    FlowOfThought: TCheckBox;
    UntreatedHighRiskMorbidity: TCheckBox;
    associations: TCheckBox;
    AffectMood: TCheckBox;
    ContentOfThought: TCheckBox;
    InsightJudgment: TCheckBox;
    ClinicalLabsReviewOrOrder: TCheckBox;
    XraysReviewOrOrder: TCheckBox;
    DiscussResultsPerformingMD: TCheckBox;
    DecideGetOldRecords: TCheckBox;
    DecideGetHxThirdParty: TCheckBox;
    ReviewSummarizeOldRecords: TCheckBox;
    UntreatedModRiskMortality: TCheckBox;
    GetHxThirdParty: TCheckBox;
    DiscussCaseOtherDocTherapist: TCheckBox;
    ReviewImageSpecimenMyself: TCheckBox;
    TimeMostlyCounseling: TCheckBox;
    RiskChronicStableOne: TCheckBox;
    RiskChronicMildlyWorse: TCheckBox;
    RiskChronicStableTwoPlus: TCheckBox;
    NewProblemNoDxNoProg: TCheckBox;
    RiskAcuteIllnessSystemicSx: TCheckBox;
    RiskChronicIllnessSeverelyWorse: TCheckBox;
    RiskHighSevereLongDisability: TCheckBox;
    RiskThreatensLifeBodilyFn: TCheckBox;
    RiskManageRxCloseMonitoring: TCheckBox;
    TimeMostlyCoordCare: TCheckBox;
    BP: TCheckBox;
    Pulse: TCheckBox;
    VS_height: TCheckBox;
    Weight: TCheckBox;
    Respiration: TCheckBox;
    Temperature: TCheckBox;
    SupineBP: TCheckBox;
    RiskManageRxDrugs: TCheckBox;
    PMH_PSH: TCheckBox;
    FamilyHx: TCheckBox;
    SocialHx: TCheckBox;
    GAB: TCheckBox;
    PFSH: TCheckGroup;
    Neuro: TCheckGroup;
    MSE: TCheckGroup;
    DataElements: TCheckGroup;
    RiskElements: TCheckGroup;
    TimeCounselCoord: TCheckGroup;
    VitalSigns: TCheckGroup;
    AttendingTimeLabel: TLabel;
    HistoryLabel: TLabel;
    ExamLabel: TLabel;
    ProblemsLabel: TLabel;
    OldProblemsLabel: TLabel;
    NewProblemsLabel: TLabel;
    ProgramTitle: TLabel;
    HPI_elements_1to3: TRadioButton;
    ProblemsMinorZero: TRadioButton;
    ProblemsMinorOne: TRadioButton;
    ProblemsMinorTwoPlus: TRadioButton;
    HPI_elements_4plus: TRadioButton;
    ProblemsOldStableZero: TRadioButton;
    ProblemsOldStableOne: TRadioButton;
    ProblemsOldStableTwo: TRadioButton;
    ProblemsOldWorseZero: TRadioButton;
    ProblemsOldWorseOne: TRadioButton;
    ProblemsOldWorseTwoPlus: TRadioButton;
    ProblemsOldStableThree: TRadioButton;
    ProblemsOldStableFourPlus: TRadioButton;
    ProblemsNewZero: TRadioButton;
    ProblemsNewNoMoreWorkup: TRadioButton;
    HPI_chronic_1to2: TRadioButton;
    ProblemsNewPlanMoreWorkup: TRadioButton;
    HPI_chronic_3plus: TRadioButton;
    VisitTypeNewPt: TRadioButton;
    VisitTypeEstablishedPt: TRadioButton;
    ROS_1: TRadioButton;
    ROS_2to9: TRadioButton;
    ROS_10to14: TRadioButton;
    HPI_elements: TRadioGroup;
    HPI_chronic_conditions: TRadioGroup;
    VisitType: TRadioGroup;
    ROS_number_systems: TRadioGroup;
    MinorProblemsNumber: TRadioGroup;
    StableProblemsNumber: TRadioGroup;
    UnstableProblemsNumber: TRadioGroup;
    NewProblemsNumber: TRadioGroup;
    procedure FaceToFaceMinChooserClick(Sender: TObject);
    procedure FaceToFaceMinutesEditingDone(Sender: TObject);
    procedure ShowMeButtonClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure HistoryLabelClick(Sender: TObject);
    procedure ProgramTitleClick(Sender: TObject);
    procedure HPI_elementsClick(Sender: TObject);
    procedure ClearAllEntriesClick(Sender: TObject);
  private
    { private declarations }
  public
    { public declarations }
  end;

var
  BillCorrectlyEntryForm: TBillCorrectlyEntryForm;

implementation

{$R *.lfm}

var
  BillByTime, ErrorNoTimeSelected, ErrorNoVisitType : Boolean;
  TimeNum, FtoFIndex : Integer;


function MedianABC ( A, B, C: Integer ) : Integer;
//"The else is always matched with the most recent if"--(wiki.freepascal.org/IF)
  begin
    if ((A>=B) and (A>=C)) then // A's biggest, so choose the higher of the other two
      if B>=C then MedianABC := B else MedianABC := C
    else if ((B>=A) and (B>=C)) then  // B's biggest
      if (A>=C) then MedianABC := A else MedianABC := C
    else  // C's biggest
      if (A>=B) then MedianABC := A else MedianABC := B;
  end; // function MedianABC

function BillingCode(): Integer;
type
  HPIType  = ( HPINil, HPIBrief, HPIExtended );
  PFSHType = ( PFSHPertinent, PFSHComplete );
  ROSType  = ( ROSProbPert, ROSExtended, ROSComplete );
     HxType   = ( HxNil,     HxPF,   HxEPF,   HxDET,   HxCOMP );
     ExamType = ( ExamNil, ExamPF, ExamEPF, ExamDET, ExamCOMP );
//     RiskType = ( RiskMinimal, RiskLow, RiskModerate, RiskHigh );
//  MDMType  = ( MDMSf, MDMLow, MDMModerate, MDMHigh );
var
  TimeBased : Boolean;
  HPIme : HPIType; ROSme : ROSType; PFSHme : PFSHType;
  HxMe : HxType; ExamMe : ExamType; {RiskMe : RiskType; MDMMe : MDMType;}
  PFSHnum, VSnum, ExamConstNum, ExamMuscNum, ExamPsyNum,
    ExamBullets, ProblemPoints, DataPoints, RiskNum,
    HxNum, ExamNum, MDMNum, {FaceToFaceMin,} Temp: Integer;
  HxMeString, ExamMeString, RiskString, MDMString: string;
begin // function BillingCode()
with BillCorrectlyEntryForm do
begin
  if (NOT VisitTypeNewPt.Checked AND NOT VisitTypeEstablishedPt.Checked) then
  begin
      // *** Need to tell user what to do here ***
      ErrorNoVisitType := True;
      BillingCode := 0;
      Exit;  // from BillingCode()
  end;
// set HPIMe, PFSHme, ROSme
  if (HPI_elements_4plus.Checked or HPI_chronic_3plus.Checked) then
     HPIme := HPIExtended
  else if (HPI_elements_1to3.Checked or HPI_chronic_1to2.Checked) then
     HPIme := HPIBrief
  else HPIMe := HPINil;
  PFSHnum := 0;
     if PMH_PSH.Checked  then Inc(PFSHnum);
     if FamilyHx.Checked then Inc(PFSHnum);
     if SocialHx.Checked then Inc(PFSHnum);
  if ( PFSHnum = 3 ) then
     PFSHme := PFSHComplete
  else if ((PFSHnum = 2) and BillCorrectlyEntryForm.VisitTypeNewPt.Checked) then
     PFSHMe := PFSHPertinent;
  if ROS_10to14.Checked    then ROSme := ROSComplete
  else if ROS_2to9.Checked then ROSme := ROSExtended
  else if ROS_1.Checked    then ROSme := ROSProbPert;

// set HxMe
  if not ChiefComplaint.Checked then HxMe := HxNil else
     begin
     if ((HPIme = HPIExtended) and (PFSHme = PFSHComplete) and
         (ROSme = ROSComplete)) then
        HxMe := HxCOMP
     else if ((HPIme = HPIExtended) and (PFSHme >= PFSHPertinent) and
               (ROSme >= ROSExtended)) then
        HxMe := HxDET
     else if ((HPIme >= HPIBrief) and (ROSme >= ROSProbPert)) then
        HxMe := HxEPF
     else if (HPIme >= HPIBrief) then
        HxMe := HxPF;
     end;  // set HxMe
  case HxMe of
    HxNil:  HxMeString := '**NONE**';
    HxCOMP: HxMeString := 'COMP';
    HxDET:  HxMeString := 'DET';
    HxEPF:  HxMeString := 'EPF';
    HxPF:   HxMeString := 'PF';
  end;

// set up for getting ExamMe
  VSnum := 0;
     if BP.Checked         then Inc(VSnum);
     if Pulse.Checked      then Inc(VSnum);
     if VS_height.Checked  then Inc(VSnum);
     if Weight.Checked     then Inc(VSnum);
     if Respiration.Checked then Inc(VSnum);
     if Temperature.Checked then Inc(VSnum);
     if SupineBP.Checked    then Inc(VSnum);
  ExamConstNum := 0;
     if (VSnum >= 3) then Inc(ExamConstNum);
     if GAB.Checked  then Inc(ExamConstNum);
  ExamMuscNum := 0;
     if StrengthTone.Checked then Inc(ExamMuscNum);
     if (Gait.Checked and Station.Checked) then Inc(ExamMuscNum);
  ExamPsyNum := 0;
     if speech.Checked then Inc(ExamPsyNum);
     if associations.Checked then ExamPsyNum := ExamPsyNum + 2; // associations and thought process each count for one bullet
     if ContentOfThought.Checked then Inc(ExamPsyNum);
     if InsightJudgment.Checked then Inc(ExamPsyNum);
     if orientation.Checked then Inc(ExamPsyNum);
     if STM_LTM.Checked then Inc(ExamPsyNum);
     if attention.Checked then Inc(ExamPsyNum);
     if language.Checked then Inc(ExamPsyNum);
     if intellect_FoK.Checked then Inc(ExamPsyNum);
     if AffectMood.Checked then Inc(ExamPsyNum);
// set ExamMe
   ExamBullets := ExamConstNum + ExamMuscNum + ExamPsyNum;
   ExamMe := ExamNil;
   if (ExamBullets >= 1) then ExamMe := ExamPF;
   if (ExamBullets >= 6) then ExamMe := ExamEPF;
   if (ExamBullets >= 9) then ExamMe := ExamDET;
   if ((ExamConstNum = 2) and (ExamMuscNum >= 1) and (ExamPsyNum = 11)) then
     ExamMe := ExamCOMP;
   case ExamMe of
     ExamNil:  ExamMeString := '**NONE**';
     ExamPF:   ExamMeString := 'PF';
     ExamEPF:  ExamMeString := 'EPF';
     ExamDET:  ExamMeString := 'DET';
     ExamCOMP: ExamMeString := 'COMP';
   end;

// set MDMme to one of: MDMSf, MDMLow, MDMModerate, MDMHigh
   ProblemPoints := 0;
   if ProblemsMinorOne.Checked then Inc(ProblemPoints)
   else if ProblemsMinorTwoPlus.Checked then ProblemPoints := ProblemPoints + 2;
   if ProblemsOldStableOne.Checked then ProblemPoints := ProblemPoints + 1
   else if ProblemsOldStableTwo.Checked then ProblemPoints := ProblemPoints + 2
   else if ProblemsOldStableThree.Checked then ProblemPoints := ProblemPoints + 3
   else if ProblemsOldStableFourPlus.Checked then
      ProblemPoints := ProblemPoints + 4;
   if ProblemsOldWorseOne.Checked then Inc(ProblemPoints)
   else if ProblemsOldWorseTwoPlus.Checked then
      ProblemPoints := ProblemPoints + 2;
   if ProblemsNewNoMoreWorkup.Checked then
      ProblemPoints := ProblemPoints + 3;
   if ProblemsNewPlanMoreWorkup.Checked then
      ProblemPoints := ProblemPoints + 4;
   // note: if ProblemPoints = 0 or = 1, either works out the same in MedianABC()
   if ProblemPoints > 4 then ProblemPoints := 4;  // maximum of 4 points

   DataPoints := 0;
   if ClinicalLabsReviewOrOrder.Checked then Inc(DataPoints);
   if XraysReviewOrOrder.Checked then Inc(DataPoints);
   if DiscussResultsPerformingMD.Checked then Inc(DataPoints);
   if (DecideGetOldRecords.Checked or DecideGetHxThirdParty.Checked) then
      Inc(DataPoints);
   if (ReviewSummarizeOldRecords.Checked or GetHxThirdParty.Checked
         or DiscussCaseOtherDocTherapist.Checked) then
       DataPoints := DataPoints + 2;
   if ReviewImageSpecimenMyself.Checked then
       DataPoints := DataPoints + 2;
   // note: if DataPoints = 0 or = 1, either works out the same in MedianABC()
   if (DataPoints > 4) then DataPoints := 4;  // maximum of 4 points

// set RiskMe to one of RiskMinimal, RiskLow, RiskModerate, RiskHigh
   if (RiskChronicIllnessSeverelyWorse.Checked or RiskThreatensLifeBodilyFn.Checked
        or UntreatedHighRiskMorbidity.Checked or UntreatedModRiskMortality.Checked
        or RiskHighSevereLongDisability.Checked
        or RiskManageRxCloseMonitoring.Checked) then
      RiskNum := 4   // Level of Risk = High
   else if (RiskChronicMildlyWorse.Checked or RiskChronicStableTwoPlus.Checked
        or NewProblemNoDxNoProg.Checked or RiskAcuteIllnessSystemicSx.Checked
        or RiskManageRxDrugs.Checked) then
      RiskNum := 3   // Level of Risk = Moderate
   else if (ProblemsMinorTwoPlus.Checked or RiskChronicStableOne.Checked
        or AcuteUncomplicatedIllness.Checked or OTCdrugs.Checked) then
      RiskNum := 2   // Level of Risk = Low
   else
      RiskNum := 1;  // Level of Risk = Minimal
   case RiskNum of
     1 :  RiskString := 'Minimal';
     2 :  RiskString := 'Low';
     3 :  RiskString := 'Moderate';
     4 :  RiskString := 'High';
   end;

// set MDMnum
   MDMNum := 1 + MedianABC( ProblemPoints, DataPoints, RiskNum );
   case MDMNum of
     1 .. 2 :  MDMString := 'Straightforward';
     3      :  MDMString := 'Low';
     4      :  MDMString := 'Moderate';
     5      :  MDMString := 'High';
   end;

   if VisitTypeNewPt.Checked then
   begin
     HxNum := 1;  ExamNum := 1;
     if (HxMe = HxCOMP) then HxNum := 5;
     if (HxMe = HxDET)  then HxNum := 3;
     if (HxMe = HxEPF)  then HxNum := 2;
     if (ExamMe = ExamCOMP) then ExamNum := 5;
     if (ExamMe = ExamDET)  then ExamNum := 3;
     if (ExamMe = ExamEPF)  then ExamNum := 2;
   end
   else // if VisitTypeEstablishedPt
   begin
     HxNum := 2;  ExamNum := 2;
     if (HxMe = HxCOMP) then HxNum := 5;
     if (HxMe = HxDET)  then HxNum := 4;
     if (HxMe = HxEPF)  then HxNum := 3;
     if (ExamMe = ExamCOMP) then ExamNum := 5;
     if (ExamMe = ExamDET)  then ExamNum := 4;
     if (ExamMe = ExamEPF)  then ExamNum := 3;
   end;

 {$IFDEF DEBUG}
    with BillThisEtcForm.DebuggingInfoMemo do
    begin
       Clear;
       Append('History: ' + HxMeString);
       Append('Exam: ' + ExamMeString);
       Append('MDM: ' + MDMString);
       Append('  (' + IntToStr(ProblemPoints) + ' Problem Points; ' +
           IntToStr(DataPoints) + ' Data Points; ' +
           'Risk ' + RiskString + ')');
    end;
 {$ENDIF}

// return BillingCode depending on new vs established pt visit
  TimeBased := (TimeMostlyCounseling.Checked or TimeMostlyCoordCare.Checked);
  FtoFIndex:=FaceToFaceMinChooser.ItemIndex;
  if (TimeBased and (FtoFIndex<=0)) then
  begin
    ErrorNoTimeSelected := True;
    BillingCode := 0;
    Exit;  // from BillingCode()
  end;
  if VisitTypeNewPt.Checked then
  // this is a new patient visit
  begin
//  The following 3 lines mean:  Temp := Min(HxNum, ExamNum, MDMnum);
     if ((HxNum <= ExamNum) and   (HxNum <= MDMnum)) then Temp :=   HxNum;
     if ((ExamNum <= HxNum) and (ExamNum <= MDMnum)) then Temp := ExamNum;
     if ((MDMNum <= ExamNum) and ( MDMnum <= HxNum)) then Temp :=  MDMNum;
     TimeNum := 0;
     if TimeBased then
     begin
       if (FtoFIndex >= 1) {10+ min.} then TimeNum := 2;
       if (FtoFIndex >= 4) {30+ min} then TimeNum := 3;
       if (FtoFIndex >= 6) {45+ min} then TimeNum := 4;
       if (FtoFIndex >= 7) {60+ min} then TimeNum := 5;
     end;
     if (TimeNum > Temp) then
     begin
       BillByTime := True;
       BillingCode := 99200 + TimeNum;
     end
     else  // either wasn't TimeBased, or no need to bill by time
     begin
       BillByTime := False;
       BillingCode := 99200 + Temp;
     end;
  end
  else // this is an established patient visit
  begin
     Temp := MedianABC(HxNum, ExamNum, MDMnum);
     TimeNum := 0;
     if TimeBased then
     begin
       if (FtoFIndex >= 1) {10+ min.} then TimeNum := 2;
       if (FtoFIndex >= 2) {15+ min} then TimeNum := 3;
       if (FtoFIndex >= 3) {25+ min} then TimeNum := 4;
       if (FtoFIndex >= 5) {40+ min} then TimeNum := 5;
     end;
     if (TimeNum > Temp) then
     begin
       BillByTime := True;
       BillingCode := 99210 + TimeNum;
     end
     else  // either wasn't TimeBased, or no need to bill by time
     begin
       BillByTime := False;
       BillingCode := 99210 + Temp;
     end;
  end;   // established patient visit
end;  // with BillCorrectlyEntryForm
end;  // function BillingCode

{ TBillCorrectlyEntryForm }

procedure TBillCorrectlyEntryForm.ProgramTitleClick(Sender: TObject);
begin

end;

procedure TBillCorrectlyEntryForm.HPI_elementsClick(Sender: TObject);
begin

end;

procedure TBillCorrectlyEntryForm.HistoryLabelClick(Sender: TObject);
begin

end;

procedure TBillCorrectlyEntryForm.FormCreate(Sender: TObject);
begin

end;

procedure TBillCorrectlyEntryForm.ShowMeButtonClick(Sender: TObject);
var
  TempString:String;
begin
  with BillThisEtcForm do
  begin
    DocumentationHintsMemo.Clear;
    if ErrorNoVisitType then
    begin
      BillThisLabel.Caption:= 'ERROR';
      DocumentationHintsMemo.Append(
        'You have to choose New patient or Established patient.');
      DocumentationHintsMemo.Append('Select one, then try again.');
      Exit; // procedure TBillCorrectlyEntryForm.ShowMeButtonClick
    end;
    if ErrorNoTimeSelected then
    begin
      BillThisEtcForm.BillThisLabel.Caption:= 'ERROR';
      DocumentationHintsMemo.Append(
        'You checked a Time box but did not select a visit duration.');
      DocumentationHintsMemo.Append('Select one, then try again.');
      Exit; // procedure TBillCorrectlyEntryForm.ShowMeButtonClick
    end;
    // No errors identified.
    TempString := 'Bill this code: ' +  IntToStr(BillingCode());
    BillThisLabel.Caption:=TempString;
    DocumentationHintsMemo.Append('Make sure you document everything you did.');
    if BillByTime then
    begin
      DocumentationHintsMemo.Append('Note in your documentation that ');
      DocumentationHintsMemo.Append(
        'Most of this visit (0:00-0:00) was spent in ');
      if TimeMostlyCoordCare.Checked  then DocumentationHintsMemo.Append(
         'coordinating care with [Dr. ___ | PT, OT | NH | pharmacy].')
      else if TimeMostlyCounseling.Checked then DocumentationHintsMemo.Append(
         'counseling the patient [and/or family].');
      DocumentationHintsMemo.Append('You said you spent ' +
        FacetoFaceMinChooser.Items.ValueFromIndex[FaceToFaceMinChooser.ItemIndex]
        + ' on this visit.');
    end;
  end; // with BillThisEtcForm
{  BillThisEtcForm.Position:=; }
  BillThisEtcForm.Show;
end;

procedure TBillCorrectlyEntryForm.FaceToFaceMinutesEditingDone(Sender: TObject);
begin

end;

procedure TBillCorrectlyEntryForm.FaceToFaceMinChooserClick(Sender: TObject);
begin

end;

procedure TBillCorrectlyEntryForm.ClearAllEntriesClick(Sender: TObject);
var
  i : integer;
{$IFDEF DEBUG}   TempString : string; {$ENDIF}
begin
with BillCorrectlyEntryForm do
begin
  for i := 0 to ComponentCount - 1 do   // for each item on Form,
  begin
{$IFDEF DEBUG}     TempString := IntToStr(i) + ' ' + Components[i].Name; {$ENDIF}
    if Components[i] is TCheckbox then
       TCheckbox(Components[i]).Checked := false;
    if Components[i] is TRadioButton then
       TRadioButton(Components[i]).Checked := false;
  end;
end; // with BillCorrectlyEntryForm
end; // procedure TBillCorrectlyEntryForm.ClearAllEntriesClick


end.

