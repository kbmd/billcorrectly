program BillCorrectly;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, BillCorrectlyEntry, BillThisEtc;

{$R *.res}

begin
  RequireDerivedFormResource := True;
  Application.Initialize;
  Application.CreateForm(TBillCorrectlyEntryForm, BillCorrectlyEntryForm);
  Application.CreateForm(TBillThisEtcForm, BillThisEtcForm);
  Application.Run;
end.

